/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankingbalanceapplication;

import static bankingbalanceapplication.MainApp.conn;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author LmThu
 */
public class ManageCustomer {

    private String customer_id;

    protected void manageCustomer() {
        viewCustomer();
        System.out.println("Please Select Option");
        int selection = ViewMainApp.getUserInput("1. Browse Accounts | 2. Make a Payment | 3. DELETE ACCOUNT |4. Update Customer Info", 1, 4);
        if (selection == 1) {
            viewAccount();
        } else if (selection == 2) {
            System.out.println("Make a Payment");
            makePayment();
        } else if (selection == 3) {
            System.out.println("Clear Credit History");
            deleteAccount();
        } else if (selection == 4) {
            System.out.println("Update Customer");
            updateCustomer();

        }

    }

    private void viewCustomer() {
        System.out.println("Search for customer by name ");
        String custName = ViewMainApp.getUserInput("Enter Name here");
        String sql = "SELECT customer_id,customer_name from customer WHERE customer_name LIKE '%" + custName + "%' ";

        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("Customer ID \t|\t Name");
            while (rs.next()) {
                System.out.print(rs.getString("customer_id") + "\t|\t");
                System.out.println(rs.getString("customer_name"));
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        System.out.println("Please Select the customer by entering the customer ID");
        String custID = ViewMainApp.getUserInput("Enter here");
        sql = "SELECT * from customer WHERE customer_id= '" + custID + "' ";
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                customer_id = rs.getString("customer_id");
                System.out.print("Customer No: " + customer_id + "\t|\t");
                System.out.println("Customer Name:" + rs.getString("customer_name"));
                System.out.print("Date Of Birth: " + rs.getString("date_of_birth"));
                System.out.println("\t| Gender: " + ((rs.getString("gender").equals("F")) ? "Female" : "Male"));
                System.out.print("Phone: " + rs.getString("phone_number"));
                System.out.print("\t|Address: " + rs.getString("address"));
            } else {
                System.out.println("No customer were found");
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    private void viewAccount() {
        Statement stmt = null;
        String sql = "SELECT account_number ,account_type,balance from account,customer WHERE "
                + "customer.customer_id=account.customer_id AND customer.customer_id= '" + customer_id + "' ";
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("Account Number | Account Type | Balance");
            while (rs.next()) {
                System.out.println(rs.getString("account_number") + "\t|" + rs.getString("account_type") + "\t\t|" + rs.getDouble("balance"));
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void makePayment() {
        String beneficiary_name = ViewMainApp.getUserInput("Please Enter the name of beneficiary");
        String ledgerId = null;
        Statement stmt = null;
        String sql = "SELECT transaction_id,transaction_category from sale_ledger,debit_transaction WHERE "
                + "sale_ledger.ledger_id =debit_transaction.ledger_id AND ledger_name='" + beneficiary_name + "'";
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("ID\t|\tCategory");
            while (rs.next()) {
                System.out.println(rs.getString("transaction_id") + "\t|\t" + rs.getString("transaction_category"));

            }
            rs.close();
            stmt.close();
            String trans = ViewMainApp.getUserInput("PLEASE ENTER THE TRANSACTION CODE: ");
            String account_number = ViewMainApp.getUserInput("PLEASE ENTER YOUR ACCOUNT NUMBER TO DEBIT ON: ");
            String description = ViewMainApp.getUserInput("REFERENCE: ");
            double amount = ViewMainApp.getUserInput("AMOUNT: ", 0, 5000);
            PreparedStatement pStatement = null;
            sql = "INSERT INTO debit_transaction_record VALUES (?,?,current_timestamp,?,?)";
            pStatement = conn.prepareStatement(sql);
            pStatement.setString(1, trans);
            pStatement.setString(2, account_number);
            pStatement.setString(3, ("IB_" + description));
            pStatement.setDouble(4, amount);
            pStatement.execute();
            pStatement.close();

            sql = "UPDATE account set balance=balance- ? WHERE account_number=?";
            pStatement = conn.prepareStatement(sql);
            pStatement.setDouble(1, amount);
            pStatement.setString(2, account_number);
            pStatement.execute();
            pStatement.close();

            viewAccount();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    private void updateCustomer() {
        System.out.println("Please Enter the new fields, press enter if you dont wish to change the field information");
        String phone_number = ViewMainApp.getUserInput("Please Enter new phone number");
        String address = ViewMainApp.getUserInput("Please Enter new address");

        if (!phone_number.equals("") || !address.equals("")) {
            String sql = "UPDATE customer SET";
            if (!phone_number.equals("")) {
                sql += " phone_number='" + phone_number + "'";
            }
            if (!address.equals("")) {
                sql += " address='" + address + "'";
            }
            sql += " WHERE customer_id='" + customer_id + "' ";
            Statement stmt = null;
            try {
                stmt = conn.createStatement();
                stmt.executeUpdate(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void deleteAccount() {
        Statement stmt = null;
        String accountNumber = ViewMainApp.getUserInput("PLEASE ENTER YOUR ACCOUNT NUMBER");
        String sql = "";
        try {
            sql = "DELETE FROM credit_transaction WHERE account_number= ? AND customer_id= ?";
            PreparedStatement pStatement = null;
            pStatement = conn.prepareStatement(sql);
            pStatement.setString(1, accountNumber);
            pStatement.setString(1, customer_id);

            pStatement.execute();
            pStatement.close();

            viewAccount();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
