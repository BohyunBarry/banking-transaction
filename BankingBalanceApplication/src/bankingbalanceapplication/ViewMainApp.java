/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankingbalanceapplication;

import java.util.Scanner;

/**
 *
 * @author LmThu
 */
public class ViewMainApp {

    static Scanner kb = new Scanner(System.in);

    public static void printWelcomeMessage() {
        System.out.println("WELCOME TO THE TBVK Banking System");

    }

    public static String getUserName() {
//        Scanner kb = new Scanner(System.in);
        System.out.println("First, Please Login");
        System.out.println("Please Enter your User Name");
        String name = kb.nextLine();
        return name;
    }

    public static String getUserPassword() {
        System.out.println("Please Enter your Password");
        String pass = kb.nextLine();
        return pass;
    }

    public static int getSelection() {
        System.out.println("Please Select the task that you want to do");
        System.out.print("1. Manage Customer");
        System.out.print("\t2. Browse Transaction");
        System.out.print("\t3. Manage Transaction Category");
        System.out.print("\t4. Exit  ->");
        int sel=kb.nextInt();
        kb.nextLine();
        return sel;
    }

    public static String getUserInput(String message) {
        System.out.println(message + ": ");
        String uInp=kb.nextLine();
        return uInp;
    }

    public static int getUserInput(String message, int lo, int hi) {
        int uInput;
        do {
            System.out.println(message);
            uInput=kb.nextInt();
            kb.nextLine();
        } while (uInput < lo || uInput > hi);
        return uInput;
    }
     public static double getUserInput(String message, double lo, double hi) {
        double uInput;
        do {
            System.out.println(message);
            uInput=kb.nextDouble();
            kb.nextLine();
        } while (uInput < lo || uInput > hi);
        return uInput;
    }


}
