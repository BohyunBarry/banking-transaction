/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankingbalanceapplication;

import java.sql.*;

/**
 *
 * @author LmThu
 */
public class MainApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ViewMainApp.printWelcomeMessage();
        MainApp ma = new MainApp();
        ma.dbConnection();
        ma.exec();
    }

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/bank_db";
    //  Database credentials
    static final String USER = ViewMainApp.getUserName();
    static final String PASS = ViewMainApp.getUserPassword();
    static Connection conn = null;
    Statement stmt = null;

    public void dbConnection() {
        try {
            // Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            // Open a connection
            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        }
        System.out.println("Goodbye!");
    }

    public void closeConnection() {
        try {
            if (stmt != null) {
                conn.close();
                System.out.println("Closed");
            }
        } catch (SQLException se) {
        }// do nothing
        try {
            if (conn != null) {
                conn.close();
                System.out.println("Closed");
            }
        } catch (SQLException se) {
            se.printStackTrace();
        }//end try
    }

    public void exec() {
        int selection;
        do {
            selection = ViewMainApp.getSelection();

            if (selection == 1) {
                ManageCustomer cust1 = new ManageCustomer();
                cust1.manageCustomer();
            } else if (selection == 2) {
                ManageTransaction mT = new ManageTransaction();
                mT.browseTransaction();
            } else if (selection == 3) {
                ManageLedger mLedger = new ManageLedger();
                
            }
        } while (selection != 4);
        System.out.println("You are about to log off from the System");
        closeConnection();
        System.out.println("Good Bye");
    }

}
