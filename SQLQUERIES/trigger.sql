DELIMITER $$
DROP TRIGGER IF EXISTS insert_message_on_delete_account $$
DROP TRIGGER IF EXISTS insert_message_on_add_account $$
DROP TRIGGER IF EXISTS update_balance_on_making_payment $$
DROP TRIGGER IF EXISTS update_balance_on_credit_payment $$

CREATE TRIGGER insert_message_on_delete_account BEFORE DELETE ON account
	FOR EACH ROW
	BEGIN
	INSERT INTO transaction_log 
		VALUES (NULL, CONCAT(OLD.account_number,': is deleted'), CURRENT_TIMESTAMP);
	END$$

CREATE TRIGGER insert_message_on_add_account BEFORE INSERT ON account
	FOR EACH ROW
	BEGIN
	INSERT INTO transaction_log 
		VALUES (NULL, CONCAT(NEW.account_number,': is add ',), CURRENT_TIMESTAMP);
	END$$
	
CREATE TRIGGER insert_message_on_update_balance AFTER UPDATE ON account
	FOR EACH ROW
	BEGIN
	INSERT INTO transaction_log 
		VALUES (NULL, CONCAT(OLD.balance,': is changed to',NEW.balance), CURRENT_TIMESTAMP);
	END$$

--- THESE TWO BELOW SHOULD BE DROP WHENEVER THE RELATED PROCEDURES ARE COMPILED OR USING SQL TRANSACTION CREATED
CREATE TRIGGER update_balance_on_making_payment AFTER INSERT ON debit_transaction_record
	FOR EACH ROW
	BEGIN
		UPDATE account set balance=balance-NEW.amount where account_number=NEW.account_number;
	END$$
	
CREATE TRIGGER update_balance_on_credit_payment AFTER INSERT ON credit_transaction
	FOR EACH ROW
	BEGIN
			UPDATE account set balance=balance+NEW.amount where account_number=NEW.account_number;
	END$$

	
DELIMITER ;