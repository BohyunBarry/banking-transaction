
/* CREATE INDEX on account table */
CREATE INDEX AccountIndex ON account (account_number);

/* CREATE Index on account table using balance columns */
CREATE INDEX AccountIndexBalance ON account (balance);

/* CREATE Index on debit_transaction table */
CREATE INDEX TransactionTypeIndex ON debit_transaction(transaction_id, ledger_id);

/* CREATE Index on debit_transaction table */
CREATE INDEX TransactionIndex ON debit_transaction_record(transaction_id, account_number);

/* CREATE Index on  Date of transaction*/
CREATE INDEX TransactionIndexOnDate ON debit_transaction_record(transaction_date);

/* CREATE Index on debit_transaction table on account_number */
CREATE INDEX TransactionAccountIndex ON debit_transaction_record(account_number);

/* DROP ALL INDEXES */

DROP INDEX AccountIndex ON account;
DROP INDEX AccountIndexBalance ON account;
DROP INDEX TransactionTypeIndex ON debit_transaction;
DROP INDEX TransactionIndex ON debit_transaction_record;
DROP INDEX TransactionIndexOnDate ON debit_transaction_record;
DROP INDEX TransactionAccountIndex ON debit_transaction_record;
