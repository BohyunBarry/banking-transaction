---ACCOUNT
/*add a new account detail*/
insert into account(account_number,customer_id,account_type,balance) values("9756203571","AA021","Student","1500");

/*update the balance*/
update account set balance="1000" where account_number="9302688509";
/*change the account type from Normal to Saving*/
update account set account_type="Saving" where account_number="9290598108" and account_type="Normal";
/*update the balance*/
update account set balance="7000" where customer_id=(select customer_id from customer where customer_name="Jennifer Sanders") and balance="6841";

/*delete the account number and balance*/
delete from account where account_number="9679486991" and balance="197";
 
/*list the sum of balance which customer_id="AA005"*/
select sum(balance) from account where customer_id="AA005";
/*find out the min and max */
select min(balance), max(balance) from account;
/*list the total number of account*/
select count(*) from account;
/*group by the customer_id, list the min(balance) of each customer*/
select customer_id, min(balance) from account group by customer_id;---
/*list the account_number and max_balance*/
select account_number,max(balance) from account group by account_number;
/*list the femal number of account*/
select customer_name, account.customer_id,count(account.account_number) from account, customer where account.customer_id=customer.customer_id group by account.customer_id
HAVING max(gender)="F";
/* Get the number of account hold by each customer */
SELECT customer_name, customer.customer_id, count(account_number) FROM customer, account WHERE account.customer_id=customer.customer_id GROUP BY customer.customer_id;

---CUSTOMER
/*add a new customer detail*/
insert into customer(customer_id,customer_name,date_of_birth,gender,phone_number,address) values ("AA025","Hey Oh",1988-05-23,"M","885412000","5 Black Road");

/*change "Joan Diza" address*/
update customer set address="Mourne View" where customer_name="Joan Diaz";
/*change Brenda Young who lives in 29 Princes Cres the gender to M*/
update customer set gender="M" where customer_name="Brenda Young" and address="29 Princes Cres";


/*delete the id=AA0016 or the address like Orchard from customer*/
delete from customer where customer_id="AA0016" or address like "%Orchard%";

/*list everything just order by name*/
select * from customer order by customer_name;
/*use phone_number as contract, then list them*/
select phone_number as PhoneNo from customer;
/*list name and the number of name*/
select distinct(customer_name), count(distinct(customer_name)) as total from customer group by customer_name order by 2 desc;

/*list birth date and count, group by birth date*/
select distinct(date_of_birth), count(distinct(date_of_birth)) from customer group by date_of_birth;



---BLOCK_TRANSACTION
/*add a new block_transaction*/
insert into block_transaction(transaction_id, account_number) values ("TES003","9250627218");

/*change the account number"9250627218", id into TES001*/
update block_transaction set transaction_id="TES001" where account_number="9250627218" and transaction_id="TES002";
/*change the account type into ANP002*/
update block_transaction set transaction_id="ANP002" where transaction_id="ANP001";

/*delete the id is ANP001 from block_transaction*/
delete from block_transaction where transaction_id="ANP001";
/*delete id=AA0016 and the account_type="Student" from block_transaction make on the assumption that 1 person only have 1 student account*/
delete from block_transaction where account_number=(select account_number from account where customer_id="AA0016" and account_type="Student");


---CREDIT_TRANSACTION
/*add a new credit_transaction*/
insert into credit_transaction(transaction_id,account_number,ordering_customer,reference,ordering_date,amount) values (NULL,"9250627218","Sed Corp.","Salary","2015-12-01","666.66");

/*change customer id=AA013 the reference to Refund, which was Travel Fee*/
update credit_transaction set reference="Refund" where account_number IN (select account_number from account where customer_id="AA013") and reference="Travel Fee";

/*calculate the avg of amount of all credit transaction*/
select avg(amount) from credit_transaction;
/*find the min and max about the amount*/
select min(amount),max(amount) from credit_transaction;
/*calculate the total of amount, who's ordering_customer like %PC*/
select sum(amount) from credit_transaction where ordering_customer like "%PC";
/*calcualte the min of amount of money on each date*/
select ordering_date,min(amount) from credit_transaction group by ordering_date;

/*list everything from credit_transaction, and order by amount*/
select * from credit_transaction order by amount asc;
/*list the total amount by every ordering customer*/
select amount from credit_transaction group by ordering_customer order by 1 desc;
/* Browse credit transaction which had been made */
SELECT * FROM credit_transaction WHERE ordering_date< CURRENT_TIMESTAMP;
/* Browse credit transaction which had not been made yet */
SELECT * FROM credit_transaction WHERE ordering_date> CURRENT_TIMESTAMP;


/*delete id is  the transaction_date=2015-10-27 and account=4000.00 from credit_transaction*/
delete from credit_transaction where transaction_id IN (select transaction_id from debit_transaction_record where transaction_date="2015-10-27" and amount="4000.00");

---DEBIT_TRANSACTION
/*add a new debit_transaction*/
insert into debit_transaction(transaction_id,ledger_id,transaction_category) values ("AA001",12,"Mobile Bill Pay");

/*change transaction_category to Mobile Bill Pay , which id is 12*/
update debit_transaction set transaction_category="Bill Pay" where ledger_id="12";
/*change transaction_category to Booking fee which id equals the ledger name is Tesco Ireland*/
update debit_transaction set transaction_category="Booking fee" where ledger_id=(select ledger_id from sale_ledger where ledger_name="Eircom");

/*delete the transaction_category=Academic fee from debit_transaction*/
delete from debit_transaction where transaction_category="Academic fee";

---DEBIT_TRANSACTION_RECORD
/*add a new debit_transaction_record*/
insert into debit_transaction_record(transaction_id,account_number,transaction_date,description,amount) values ("DKI001","9250627218","2015-12-25","POS_JJB_MEMBERSHIP","150");

/*change the transaction_date to 2016-01-01 which account_number=9250627218*/
update debit_transaction_record set transaction_date="2016-01-01" where account_number="9250627218";

/*list everything, order by transaction_date*/
select * from debit_transaction_record order by transaction_date;

/*calculate the avg with 2 decimal places*/
select format(avg(amount),2) as average_amount from debit_transaction_record;
/*list the account > the avg of amount, the account_number and account*/
select account_number, amount from debit_transaction_record where amount > (select avg(amount) from debit_transaction_record);



