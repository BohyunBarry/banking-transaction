/* Load the data from the csv files into the tables */

/* customer table */
LOAD DATA INFILE "F:/SCHOOL_WORKS/Y2/DatabaseProject/mock_data/customer_formatted.csv"
INTO TABLE customer
COLUMNS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

/* account table */
LOAD DATA INFILE "F:/SCHOOL_WORKS/Y2/DatabaseProject/mock_data/account_formatted.csv"
INTO TABLE account
COLUMNS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

/* credit_transaction table */
LOAD DATA INFILE "F:/SCHOOL_WORKS/Y2/DatabaseProject/mock_data/credit_transaction_formatted.csv"
INTO TABLE credit_transaction
COLUMNS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

/* sale ledger table */
LOAD DATA INFILE "F:/SCHOOL_WORKS/Y2/DatabaseProject/mock_data/sale_ledger_formatted.csv"
INTO TABLE sale_ledger
COLUMNS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

/* debit_transaction table */
LOAD DATA INFILE "F:/SCHOOL_WORKS/Y2/DatabaseProject/mock_data/debit_transaction_formatted.csv"
INTO TABLE debit_transaction
COLUMNS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

/* transaction_record table */
LOAD DATA INFILE "F:/SCHOOL_WORKS/Y2/DatabaseProject/mock_data/transaction_record_formatted.csv"
INTO TABLE debit_transaction_record
COLUMNS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

/* block transaction table */
LOAD DATA INFILE "F:/SCHOOL_WORKS/Y2/DatabaseProject/mock_data/block_transaction_formatted.csv"
INTO TABLE block_transaction
COLUMNS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

UPDATE bank_db.sale_ledger SET ledger_name = replace(ledger_name, '\r', '') WHERE ledger_name LIKE '%\r';

