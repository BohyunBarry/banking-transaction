/* GET CURRENT ISOLATION LEVEL */
SELECT @@tx_isolation;

/* SET ISOLATON LEVEL TO BE SERIALIZABLE */
SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
/* SET AUTO_COMMIT */
SET autocommit=1;

/* SESSION 1- T1 */


/* IF PROCEED WITHOUT transaction may have problem */
insert into debit_transaction_record(transaction_id,account_number,transaction_date,description,amount) values ("TES001","9006070182",CURRENT_TIMESTAMP,"POS_PAY_TESCO_STORE",30);
	UPDATE account set balance=balance-100 where account_number="9006070182";

DELIMITER //
DROP PROCEDURE IF EXISTS getTrans//
DROP PROCEDURE IF EXISTS getCreTrans//


CREATE PROCEDURE getTrans()
BEGIN
	DELETE FROM debit_transaction_record WHERE account_number="9006070182";
	 
	START TRANSACTION;
		insert into debit_transaction_record(transaction_id,account_number,transaction_date,description,amount) values ("TES001","9006070182",CURRENT_TIMESTAMP,"POS_PAY_TESCO_STORE",30);
		UPDATE account set balance=balance-100 where account_number="9006070182";
		SET @balance= (SELECT balance from account where account_number="9006070182");
		IF (@balance>0) THEN
			COMMIT;
		ELSE 
			ROLLBACK;
		END IF;
		 
END//

CREATE PROCEDURE getCreTrans()
BEGIN
	DELETE FROM credit_transaction WHERE account_number="9006070182";
	 
	START TRANSACTION;
		insert into credit_transaction  values (NULL,"9006070182","DKIT","refund",CURRENT_TIMESTAMP,300);
		UPDATE account set balance=balance+300 where account_number="9006070182";
		SET @accountType= (SELECT account_type from account where account_number="9006070182");
		IF (@accountType="SAVING") THEN
			ROLLBACK;
		ELSE 
			COMMIT;
		END IF;
		 
END//


DELIMITER ;

CALL getTrans();
CALL getCreTrans();
