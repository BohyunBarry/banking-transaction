DELIMITER //

DROP PROCEDURE IF EXISTS getcustomer//
DROP PROCEDURE IF EXISTS getAllCreditTransactionMakeOnDate//
DROP PROCEDURE IF EXISTS getDebitTransactionByMonth//
DROP PROCEDURE IF EXISTS getTotalNumberCustomer//
DROP PROCEDURE IF EXISTS getMaxAmountDebit//
DROP PROCEDURE IF EXISTS deductMoney//


/* GET ALL Customer */
CREATE PROCEDURE getcustomer()
BEGIN
	SELECT * FROM customer;
END//

/* GET ALL transaction make on one date */
CREATE PROCEDURE getAllCreditTransactionMakeOnDate(IN order_day date)
BEGIN
	SELECT * FROM cedit_transaction WHERE ordering_date=order_day;
END//

/* 3 */
CREATE PROCEDURE getDebitTransactionByMonth(IN trans_month int(4))
BEGIN
	SELECT * FROM debit_transaction_record WHERE MONTH(transaction_date)=trans_month;
END//

/* 4 */
CREATE PROCEDURE getTotalNumberCustomer()
BEGIN
	SELECT count(*) INTO total FROM customer;
END//

/* 5 */
CREATE PROCEDURE getMaxAmountDebit(OUT _max int(4))
BEGIN
	SELECT count(*) INTO _max FROM customer;
END//

/* 6 */
CREATE PROCEDURE deductMoney(OUT _balance int(4), IN amount int(4), IN _account char(10))
BEGIN
	SELECT balance-_amount INTO _balance FROM account WHERE account_number=_account;
END//



DELIMITER ;

CALL getAllCreditTransactionMakeOnDate("2015-01-25");
CALL getDebitTransactionByMonth(01);
CALL getTotalNumberCustomer;
CALL getMaxAmountDebit(@amount);
CALL deductMoney(new_balance, 1000, @account);
