/* Get the total list of customer */
SELECT * FROM customer;

/* Get the list of transaction categories */
SELECT * from debit_transaction;

/* GET all transaction record */
SELECT * FROM debit_transaction_record;

/* Get all credit transaction */
SELECT * FROM credit_transaction;

/* Get all block transaction */
SELECT * FROM block_transaction;

/* Get the number of account hold by each customer */
SELECT customer_name, customer.customer_id, count(account_number) FROM customer, account WHERE account.customer_id=customer.customer_id GROUP BY customer.customer_id;

/* Browse transaction by date */
SELECT * FROM debit_transaction_record ORDER BY 3 DESC;

/* Browse credit transaction which had been made */
SELECT * FROM credit_transaction WHERE ordering_date< CURRENT_TIMESTAMP;

/* Browse credit transaction which had not been made yet */
SELECT * FROM credit_transaction WHERE ordering_date> CURRENT_TIMESTAMP;

