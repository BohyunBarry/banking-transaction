
-- CUSTOMER AND ACCOUNT TABLE
/* Create the views for view customer info SIMPLE VIEW*/
CREATE VIEW customer_info_view AS 
	SELECT * FROM customer;
/* Create view for  all record in account table -- SIMPLE VIEW*/
CREATE VIEW account_info_view AS 
	SELECT * FROM account;

/* Create view for view account_count from the account table */
CREATE VIEW num_of_account_view as
SELECT count(*) FROM account;

/* Create view for  view info customer account*/
CREATE VIEW view_user_number_of_account as
SELECT customer.customer_id, customer_name, COUNT(account_number) AS num_of_account, SUM(balance) AS total_fund 
	FROM customer,account
	WHERE customer.customer_id=account.customer_id GROUP BY customer_name;




--CREDIT/BLOCK TRANSACTION AND ACCOUNT TABLE 

/* View for view all credit transaction */
CREATE VIEW AS view_all_credit_transaction AS
SELECT * FROM credit_transaction;

/* View for view all credit transaction with customer */
CREATE VIEW view_all_credit_transaction_with_customer AS
SELECT customer_name, credit_transaction.* from customer, credit_transaction,account WHERE customer.customer_id=account.customer_id 
AND account.account_number=credit_transaction.account_number;

/* View transaction which had happened */
CREATE VIEW view_credit_transaction_made AS
SELECT customer_name, credit_transaction.* from customer, credit_transaction,account WHERE customer.customer_id=account.customer_id 
AND account.account_number=credit_transaction.account_number AND ordering_date<current_timestamp; 

/* View transaction which had not happened */
CREATE VIEW view_credit_transaction_pending AS
SELECT customer_name, credit_transaction.* from customer, credit_transaction,account WHERE customer.customer_id=account.customer_id 
AND account.account_number=credit_transaction.account_number AND ordering_date>current_timestamp; 

/* View all transaction from block transaction table */

CREATE VIEW view_all_block_transaction AS
SELECT * FROM block_transaction;

/* VIEW the max and min of  credit transaction table*/
CREATE VIEW AS view_min_max_count_credit_transaction AS
SELECT min(amount) as Minimum, max(amount) as Maximum, count(amount) numberOfTrans, avg(amount) Average FROM credit_transaction;

--TRANSACTION RECORD TABLE

/* View for browsing every record from record table */
CREATE VIEW view_all_debit_transaction_record AS
SELECT * FROM debit_transaction_record;

/* VIEW the max and min  and average of debit transaction  make for a paymenttable*/
CREATE VIEW AS view_min_max_count_credit_transaction AS
SELECT transaction_category as category, min(amount) as Minimum, max(amount) as Maximum, count(amount) numberOfTrans, avg(amount) Average FROM debit_transaction_record,debit_transaction 
 WHERE debit_transaction.transaction_id=debit_transaction_record.transaction_id
GROUP BY debit_transaction.transaction_id;


	/* TEST CASES */
SELECT * FROM customer_info_view;
INSERT INTO account_info_view VALUES("9933249103","AA009","Student",1781);
SELECT * FROM num_of_account_view;

